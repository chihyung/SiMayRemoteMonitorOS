﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiMay.RemoteControlsCore
{
    public interface IScheduleTrigger
    {
        void Execute();
    }
}
